package main

import (
	"fmt"
	"log"
	"net"

	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
	"google.golang.org/grpc"

	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/book_service/config"
	pb "gitlab.com/book_market/book_service/genproto/book_service"
	grpcclient "gitlab.com/book_market/book_service/pkg/grpc_client"
	"gitlab.com/book_market/book_service/pkg/logger"
	"gitlab.com/book_market/book_service/service"
	"gitlab.com/book_market/book_service/storage"
)

func main() {
	cfg := config.Load(".")

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}

	rdb := redis.NewClient(&redis.Options{
		Addr: cfg.Redis.Addr,
	})

	strg := storage.NewStoragePg(psqlConn)
	inMemory := storage.NewInMemoryStorage(rdb)

	logrus := logger.New()
	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		logrus.Fatalf("failed to connect another microservice: %v", err)
	}

	bookService := service.NewBookService(strg, inMemory, logrus)
	authorService := service.NewAuthorService(strg, inMemory, logrus)
	categoryService := service.NewCategoryService(strg, inMemory, logrus)
	reviewService := service.NewReviewService(strg, inMemory, logrus, grpcClient)

	lis, err := net.Listen("tcp", cfg.GRPC_PORT)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()

	pb.RegisterBookServiceServer(s, bookService)
	pb.RegisterAuthorServiceServer(s, authorService)
	pb.RegisterCategoryServiceServer(s, categoryService)
	pb.RegisterReviewServiceServer(s, reviewService)

	log.Println("Grpc server started in port ", cfg.GRPC_PORT)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}
}