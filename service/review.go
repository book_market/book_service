package service

import (
	"context"
	"database/sql"
	"errors"
	"sync"

	"github.com/sirupsen/logrus"
	pb "gitlab.com/book_market/book_service/genproto/book_service"
	grpcclient "gitlab.com/book_market/book_service/pkg/grpc_client"
	"gitlab.com/book_market/book_service/storage"
	"gitlab.com/book_market/book_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ReviewService struct {
	pb.UnimplementedReviewServiceServer
	storage    storage.StorageI
	inMemory   storage.InMemoryStorageI
	logger     *logrus.Logger
	grpcClient grpcclient.GrpcClientI
}

func NewReviewService(strg storage.StorageI, inMemory storage.InMemoryStorageI, log *logrus.Logger, grpcClient grpcclient.GrpcClientI) *ReviewService {
	return &ReviewService{
		storage:    strg,
		inMemory:   inMemory,
		logger:     log,
		grpcClient: grpcClient,
	}
}

func (r *ReviewService) Create(ctx context.Context, req *pb.Review) (*pb.Review, error) {
	result, err := r.storage.Review().CreateReview(&repo.Review{
		BookId: req.BookId,
		CustomerId: req.CustomerId,
		Review: req.Review,
	})
	if err != nil {
		r.logger.WithError(err).Error("failed to create review")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	book := r.getTwoInfo(result.BookId)
	return parseReviewModel(result, book), nil
}

func (b *ReviewService) getTwoInfo(bookId int64) *repo.Book {
	var wg sync.WaitGroup
	var err error
	var book *repo.Book
	wg.Add(1)
	go func() {
		defer wg.Done()
		book, err = b.storage.Book().GetBook(bookId)
		if err != nil {
			b.logger.WithError(err).Error("failed to get book info")
		}
	}()
	
	wg.Wait()
	return book
}

func parseReviewModel(review *repo.Review, book *repo.Book) *pb.Review {
	return &pb.Review{
		Id:     review.Id,
		BookId: review.BookId,
		Book: &pb.GetBookData{
			Name:          book.Name,
			CategoryId:    book.CategoryId,
			AuthorId:      book.AuthorId,
			Description:   book.Description,
			PublisherName: book.PublisherName,
			Language:      book.Language,
			Inscription:   book.Inscription,
			Isbn:          book.Isbn,
			Page:          book.Page,
			Wrapper:       book.Wrapper,
			Format:        book.Format,
			ImageUrl:      book.ImageUrl,
			Price:         float32(book.Price),
			CreatedAt:     book.CreatedAt,
		},
		CustomerId:    review.CustomerId,
		Review:    review.Review,
		CreatedAt: review.CreatedAt,
	}
}

func (r *ReviewService) Get(ctx context.Context, req *pb.ReviewIdRequest) (*pb.Review, error) {
	result, err := r.storage.Review().GetReview(req.Id)
	if err != nil {
		r.logger.WithError(err).Error("failed to get review")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}
	book := r.getTwoInfo(result.BookId)
	return parseReviewModel(result, book), nil
}

func (r *ReviewService) GetAll(ctx context.Context, req *pb.GetAllReviewsRequest) (*pb.GetAllReviewsResponse, error) {
	result, err := r.storage.Review().GetAllReviews(&repo.GetAllReviewsParams{
		Limit:  req.Limit,
		Page:   req.Page,
		Search: req.Search,
	})
	if err != nil {
		r.logger.WithError(err).Error("failed to get all reviews")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	response := pb.GetAllReviewsResponse{
		Count:   result.Count,
		Reviews: make([]*pb.Review, 0),
	}

	for _, review := range result.Reviews {
		book := r.getTwoInfo(review.BookId)
		response.Reviews = append(response.Reviews, parseReviewModel(review, book))
	}

	return &response, nil
}

func (r *ReviewService) Delete(ctx context.Context, req *pb.ReviewIdRequest) (*emptypb.Empty, error) {
	err := r.storage.Review().DeleteReview(req.Id)
	if err != nil {
		r.logger.WithError(err).Error("failed to delete error")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return &emptypb.Empty{}, nil
}
