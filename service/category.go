package service

import (
	"context"
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"
	pb "gitlab.com/book_market/book_service/genproto/book_service"
	"gitlab.com/book_market/book_service/storage"
	"gitlab.com/book_market/book_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CategoryService struct {
	pb.UnimplementedCategoryServiceServer
	storage  storage.StorageI
	inMemory storage.InMemoryStorageI
	logger   *logrus.Logger
}

func NewCategoryService(strg storage.StorageI, inMemory storage.InMemoryStorageI, log *logrus.Logger) *CategoryService {
	return &CategoryService{
		storage:  strg,
		inMemory: inMemory,
		logger:   log,
	}
}

func (c *CategoryService) Create(ctx context.Context, req *pb.Category) (*pb.Category, error) {
	result, err := c.storage.Category().CreateCategory(&repo.Category{
		Name:     req.Name,
		ImageUrl: req.ImageUrl,
	})
	if err != nil {
		c.logger.WithError(err).Error("failed to create category")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseCategoryModel(result), nil
}

func parseCategoryModel(category *repo.Category) *pb.Category {
	return &pb.Category{
		Id:        category.Id,
		Name:      category.Name,
		ImageUrl:  category.ImageUrl,
		CreatedAt: category.CreatedAt,
	}
}

func (c *CategoryService) Get(ctx context.Context, req *pb.CategoryIdRequest) (*pb.Category, error) {
	result, err := c.storage.Category().GetCategory(req.Id)
	if err != nil {
		c.logger.WithError(err).Error("failed to get category")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseCategoryModel(result), nil
}

func (c *CategoryService) GetAll(ctx context.Context, req *pb.GetAllCategoriesRequest) (*pb.GetAllCategoriesResponse, error) {
	result, err := c.storage.Category().GetAllCategories(&repo.GetAllCategoriesParams{
		Limit:  req.Limit,
		Page:   req.Page,
		Search: req.Search,
	})
	if err != nil {
		c.logger.WithError(err).Error("failed to get all categories")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	response := pb.GetAllCategoriesResponse{
		Count:      result.Count,
		Categories: make([]*pb.Category, 0),
	}

	for _, category := range result.Categories {
		response.Categories = append(response.Categories, parseCategoryModel(category))
	}

	return &response, nil
}

func (c *CategoryService) Delete(ctx context.Context, req *pb.CategoryIdRequest) (*emptypb.Empty, error) {
	err := c.storage.Category().DeleteCategory(req.Id)
	if err != nil {
		c.logger.WithError(err).Error("failed to delete category")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return &emptypb.Empty{}, nil
}