package service

import (
	"context"
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"
	pb "gitlab.com/book_market/book_service/genproto/book_service"
	"gitlab.com/book_market/book_service/storage"
	"gitlab.com/book_market/book_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type AuthorService struct {
	pb.UnimplementedAuthorServiceServer
	storage  storage.StorageI
	inMemory storage.InMemoryStorageI
	logger   *logrus.Logger
}

func NewAuthorService(strg storage.StorageI, inMemory storage.InMemoryStorageI, log *logrus.Logger) *AuthorService {
	return &AuthorService{
		storage:  strg,
		inMemory: inMemory,
		logger:   log,
	}
}

func (a *AuthorService) Create(ctx context.Context, req *pb.Author) (*pb.Author, error) {
	a.logger.Info("create author")
	author, err := a.storage.Author().CreateAuthor(&repo.Author{
		Fullname: req.FullName,
	})
	if err != nil {
		a.logger.WithError(err).Error("failed to create author")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}
	return parseAuthorModel(author), nil
}

func parseAuthorModel(author *repo.Author) *pb.Author {
	return &pb.Author{
		Id:        author.Id,
		FullName:  author.Fullname,
		CreatedAt: author.CreatedAt,
	}
}

func (a *AuthorService) Get(ctx context.Context, req *pb.AuthorIdRequest) (*pb.Author, error) {
	author, err := a.storage.Author().GetAuthor(req.Id)
	if err != nil {
		a.logger.WithError(err).Error("failed to get author")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseAuthorModel(author), nil
}

func (a *AuthorService) GetAll(ctx context.Context, req *pb.GetAllAuthorsRequest) (*pb.GetAllAuthorsResponse, error) {
	result, err := a.storage.Author().GetAllAuthors(&repo.GetAllAuthorsParams{
		Limit:  req.Limit,
		Page:   req.Page,
		Search: req.Search,
	})
	if err != nil {
		a.logger.WithError(err).Error("failed to get all authors")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	response := pb.GetAllAuthorsResponse{
		Count: result.Count,
		Authors: make([]*pb.Author, 0),
	}

	for _, author := range result.Authors {
		response.Authors = append(response.Authors, parseAuthorModel(author))
	}

	return &response, nil 
}

func (a *AuthorService) Delete(ctx context.Context, req *pb.AuthorIdRequest) (*emptypb.Empty, error) {
	err := a.storage.Author().DeleteAuthor(req.Id)

	if err != nil {
		a.logger.WithError(err).Error("failed to delete author")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return &emptypb.Empty{}, nil
}