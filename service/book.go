package service

import (
	"context"
	"database/sql"
	"errors"
	"sync"

	"github.com/sirupsen/logrus"
	pb "gitlab.com/book_market/book_service/genproto/book_service"
	"gitlab.com/book_market/book_service/storage"
	"gitlab.com/book_market/book_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type BookService struct {
	pb.UnimplementedBookServiceServer
	storage  storage.StorageI
	inMemory storage.InMemoryStorageI
	logger   *logrus.Logger
}

func NewBookService(strg storage.StorageI, inMemory storage.InMemoryStorageI, log *logrus.Logger) *BookService {
	return &BookService{
		storage:  strg,
		inMemory: inMemory,
		logger:   log,
	}
}

func (b *BookService) Create(ctx context.Context, req *pb.Book) (*pb.Book, error) {
	result, err := b.storage.Book().CreateBook(&repo.Book{
		Name:          req.Name,
		CategoryId:    req.CategoryId,
		AuthorId:      req.AuthorId,
		Description:   req.Description,
		PublisherName: req.PublisherName,
		Language:      req.Language,
		Inscription:   req.Inscription,
		Page:          req.Page,
		Isbn:          req.Isbn,
		Format:        req.Format,
		Wrapper:       req.Wrapper,
		Price:         float64(req.Price),
		ImageUrl:      req.ImageUrl,
	})

	if err != nil {
		b.logger.WithError(err).Error("failed to create book")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	category, author := b.getTwoInfo(result.CategoryId, result.AuthorId)

	return parseBookModels(result, category, author), nil
}

func parseBookModels(book *repo.Book, c *repo.Category, a *repo.Author) *pb.Book {
	return &pb.Book{
		Id:         book.Id,
		Name:       book.Name,
		CategoryId: book.CategoryId,
		Category: &pb.GetCategoryData{
			Name:      c.Name,
			ImageUrl:  c.ImageUrl,
			CreatedAt: c.CreatedAt,
		},
		AuthorId: book.AuthorId,
		Author: &pb.GetAuthorData{
			FullName:  a.Fullname,
			CreatedAt: a.CreatedAt,
		},
		Description:   book.Description,
		PublisherName: book.PublisherName,
		Language:      book.Language,
		Inscription:   book.Inscription,
		Page:          book.Page,
		Isbn:          book.Isbn,
		Format:        book.Format,
		Wrapper:       book.Wrapper,
		Price:         float32(book.Price),
		ImageUrl:      book.ImageUrl,
		CreatedAt:     book.CreatedAt,
	}
}

func (b *BookService) getTwoInfo(categoryId, authorId int64) (*repo.Category, *repo.Author) {
	var wg sync.WaitGroup
	var err error
	var category *repo.Category
	var author *repo.Author
	wg.Add(2)
	go func() {
		defer wg.Done()
		category, err = b.storage.Category().GetCategory(categoryId)
		if err != nil {
			b.logger.WithError(err).Error("failed to get category info")
		}
	}()

	go func() {
		defer wg.Done()
		author, err = b.storage.Author().GetAuthor(authorId)
		if err != nil {
			b.logger.WithError(err).Error("failed to get author info")
		}
	}()

	wg.Wait()
	return category, author
}

func (b *BookService) Get(ctx context.Context, req *pb.BookIdRequest) (*pb.Book, error) {
	result, err := b.storage.Book().GetBook(req.Id)
	if err != nil {
		b.logger.WithError(err).Error("failed to get book")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	category, author := b.getTwoInfo(result.CategoryId, result.AuthorId)

	return parseBookModels(result, category, author), nil
}

func (b *BookService) GetAll(ctx context.Context, req *pb.GetAllBooksRequest) (*pb.GetAllBooksResponse, error) {
	result, err := b.storage.Book().GetAllBooks(&repo.GetAllBooksParams{
		Limit:         req.Limit,
		Page:          req.Page,
		Name:          req.Name,
		AuthorId:      int64(req.AuthorId),
		CategoryId:    int64(req.CategoryId),
		PublisherName: req.PublisherName,
		Language:      req.Language,
		Inscription:   req.Inscription,
		Wrapper:       req.Wrapper,
		PriceFrom:     float64(req.PriceFrom),
		PriceTo:       float64(req.PriceTo),
	})
	if err != nil {
		b.logger.WithError(err).Error("failed to get all books")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	response := pb.GetAllBooksResponse{
		Count: result.Count,
		Books: make([]*pb.Book, 0),
	}

	for _, book := range result.Books {
		category, author := b.getTwoInfo(book.CategoryId, book.AuthorId)
		response.Books = append(response.Books, parseBookModels(book, category, author))
	}

	return &response, nil
}

func (b *BookService) Delete(ctx context.Context, req *pb.BookIdRequest) (*emptypb.Empty, error) {
	err := b.storage.Book().DeleteBook(req.Id)
	if err != nil {
		b.logger.WithError(err).Error("failed to delete book")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return &emptypb.Empty{}, nil
}
