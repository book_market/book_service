drop table if exists books;
drop table if exists reviews;
drop table if exists authors;
drop table if exists categories;