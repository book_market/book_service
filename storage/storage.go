package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/book_service/storage/postgres"
	"gitlab.com/book_market/book_service/storage/repo"
)

type StorageI interface {
	Book() repo.BookStorageI
	Author() repo.AuthorStorageI
	Review() repo.ReviewStorageI
	Category() repo.CategoryStorageI
}

type storagePg struct {
	bookRepo     repo.BookStorageI
	authorRepo   repo.AuthorStorageI
	reviewRepo   repo.ReviewStorageI
	categoryRepo repo.CategoryStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		bookRepo:     postgres.NewBook(db),
		authorRepo:   postgres.NewAuthor(db),
		reviewRepo:   postgres.NewReview(db),
		categoryRepo: postgres.NewCategory(db),
	}
}

func (s *storagePg) Book() repo.BookStorageI {
	return s.bookRepo
}

func (s *storagePg) Author() repo.AuthorStorageI {
	return s.authorRepo
}

func (s *storagePg) Review() repo.ReviewStorageI {
	return s.reviewRepo
}

func (s *storagePg) Category() repo.CategoryStorageI {
	return s.categoryRepo
}
