package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/book_market/book_service/storage/repo"
)

func createBook(t *testing.T) *repo.Book {
	authorId := createAuthor(t)
	categoryId := createCategory(t)

	b, err := strg.Book().CreateBook(&repo.Book{
		Name:          faker.FirstName(),
		Description:   faker.Sentence(),
		AuthorId:      authorId.Id,
		CategoryId:    categoryId.Id,
		PublisherName: faker.Word(),
		Inscription:   faker.Word(),
		Format:        faker.Word(),
		Isbn:          faker.Word(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, b)

	return b
}

func TestCreateBook(t *testing.T) {
	createBook(t)
}

func TestGetBook(t *testing.T) {
	c := createBook(t)

	book, err := strg.Book().GetBook(c.Id)
	require.NoError(t, err)
	require.NotEmpty(t, book)
}

func TestGetAllBooks(t *testing.T) {
	book, err := strg.Book().GetAllBooks(&repo.GetAllBooksParams{
		Limit:  10,
		Page:   1,
		Name: "Sir",
		AuthorId: 1,
		CategoryId: 1,
		PublisherName: "nasim",
		Language: "bek",
		Inscription: "yu",
		Wrapper: "rill",
		PriceFrom: 50000,
		PriceTo: 100000,
	})
	require.NoError(t, err)
	require.NotEmpty(t, book)
}


func TestDeleteBook(t *testing.T) {
	c := createBook(t)
	err := strg.Book().DeleteBook(c.Id)
	require.NoError(t, err)
}
