package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/book_service/storage/repo"
)

type categoryRepo struct {
	db *sqlx.DB
}

func NewCategory(db *sqlx.DB) repo.CategoryStorageI {
	return &categoryRepo{
		db: db,
	}
}

func (b *categoryRepo) CreateCategory(category *repo.Category) (*repo.Category, error) {
	query := `
		insert into categories (
			name,
			image_url
		) values ($1, $2)
		returning id, created_at
	`

	row := b.db.QueryRow(
		query,
		category.Name,
		category.ImageUrl,
	)

	err := row.Scan(
		&category.Id,
		&category.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return category, nil
}

func (b *categoryRepo) GetCategory(id int64) (*repo.Category, error) {
	var (
		result repo.Category
	)

	query := `
		select
			id,
			name,
			image_url,
			created_at
		from categories 
		where id = $1
	`

	row := b.db.QueryRow(query, id)
	err := row.Scan(
		&result.Id,
		&result.Name,
		&result.ImageUrl,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (b *categoryRepo) GetAllCategories(params *repo.GetAllCategoriesParams) (*repo.GetAllCategoriesResults, error) {
	result := repo.GetAllCategoriesResults{
		Categories: make([]*repo.Category, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" limit %d offset %d ", params.Limit, offset)

	filter := ""
	if params.Search != "" {
		str := "%" + params.Search + "%"
		filter += fmt.Sprintf(`
			where name ilike '%s'`,
		str,
		)
	}
	
	query := `
	select	
		id,
		name,
		image_url,
		created_at
	from categories 
	` + filter + `order by created_at desc` + limit 

	rows, err := b.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var a repo.Category	
		err := rows.Scan(
			&a.Id,
			&a.Name,
			&a.ImageUrl,
			&a.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.Categories = append(result.Categories, &a)
	}

	queryCount := `select count(1) from categories ` + filter
	err = b.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (b *categoryRepo) DeleteCategory(id int64) error {
	query := `delete from categories where id = $1`

	res, err := b.db.Exec(query, id)
	if err != nil {
		return err
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}