package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/book_service/storage/repo"
)

type bookRepo struct {
	db *sqlx.DB
}

func NewBook(db *sqlx.DB) repo.BookStorageI {
	return &bookRepo{
		db: db,
	}
}

func (b *bookRepo) CreateBook(book *repo.Book) (*repo.Book, error) {
	query := `
		insert into books (
			name,
			category_id,
			author_id,
			description,
			publisher_name,
			language,
			inscription,
			page,
			isbn,
			format,
			wrapper,
			image_url,
			price
		) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
		returning id, created_at
	`

	row := b.db.QueryRow(
		query,
		book.Name,
		book.CategoryId,
		book.AuthorId,
		book.Description,
		book.PublisherName,
		book.Language,
		book.Inscription,
		book.Page,
		book.Isbn,
		book.Format,
		book.Wrapper,
		book.ImageUrl,
		book.Price,
	)

	err := row.Scan(
		&book.Id,
		&book.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return book, nil
}

func (b *bookRepo) GetBook(id int64) (*repo.Book, error) {
	var (
		result repo.Book
		category repo.Category
		author repo.Author
	)

	query := `
		select 
			b.id,
			b."name" ,
			b.category_id ,
			c."name" ,
			c.image_url ,
			c.created_at ,
			b.author_id,
			a.fullname ,
			a.created_at ,
			b.description ,
			b.publisher_name ,
			b."language" ,
			b.inscription ,
			b.page ,
			b.isbn ,
			b.format ,
			b."wrapper" ,
			b.image_url ,
			b.price ,
			b.created_at 
		from books b join categories c on c.id  = b.category_id join authors a on a.id = b.author_id 
		where b.id = $1
		`
	row := b.db.QueryRow(query, id)
	err := row.Scan(
		&result.Id,
		&result.Name,
		&result.CategoryId,
		&category.Name,
		&category.ImageUrl,
		&category.CreatedAt,
		&result.AuthorId,
		&author.Fullname,
		&author.CreatedAt,
		&result.Description,
		&result.PublisherName,
		&result.Language,
		&result.Inscription,
		&result.Page,
		&result.Isbn,
		&result.Format,
		&result.Wrapper,
		&result.ImageUrl,
		&result.Price,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (b *bookRepo) GetAllBooks(params *repo.GetAllBooksParams) (*repo.GetAllBooksResult, error) {
	result := repo.GetAllBooksResult{
		Books: make([]*repo.Book, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" limit %d offset %d ", params.Limit, offset)
	
	filter := "WHERE true"

	if params.Name != "" {
		str := "%" + params.Name + "%"
		filter += fmt.Sprintf(`
			and name ilike '%s' `, str)
	}

	if params.AuthorId != 0 {
		filter += fmt.Sprintf(` and author_id::text ilike '%d' `, params.AuthorId)
	} 

	if params.CategoryId != 0 {
		filter += fmt.Sprintf(` and category_id::text ilike '%d' `, params.CategoryId)
	} 
	
	if params.PublisherName != "" {
		str := "%" + params.PublisherName + "%"
		filter += fmt.Sprintf(`
			and publisher_name ilike '%s' `, str)
	}

	if params.Language != "" {
		str := "%" + params.Language + "%"
		filter += fmt.Sprintf(` and language ilike '%s' `, str)
	}

	if params.Inscription != "" {
		str := "%" + params.Inscription + "%"
		filter += fmt.Sprintf(` and inscription ilike '%s' `, str)
	}

	if params.Wrapper != "" {
		str := "%" + params.Wrapper + "%"
		filter += fmt.Sprintf(` and wrapper ilike '%s' `, str)
	}

	if params.PriceFrom != 0 {
		filter += fmt.Sprintf(` and price >= '%f' `, params.PriceFrom)
	} 

	if params.PriceTo != 0 {
		filter += fmt.Sprintf(` and price <= '%f' `, params.PriceTo)
	}

	query := `
	select
		id,
		name,
		category_id,
		author_id,
		description,
		publisher_name,
		language,
		inscription,
		page,
		isbn,
		format,
		wrapper,
		image_url,
		price,
		created_at
	from books  
	` + filter + ` order by created_at desc` + limit 

	rows, err := b.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var b repo.Book	
		err := rows.Scan(
			&b.Id,
			&b.Name,
			&b.CategoryId,
			&b.AuthorId,
			&b.Description,
			&b.PublisherName,
			&b.Language,
			&b.Inscription,
			&b.Page,
			&b.Isbn,
			&b.Format,
			&b.Wrapper,
			&b.ImageUrl,
			&b.Price,
			&b.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.Books = append(result.Books, &b)
	}

	queryCount := `select count(1) from books ` + filter
	err = b.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (b *bookRepo) DeleteBook(id int64) error {
	query := `delete from books where id = $1`

	res, err := b.db.Exec(query, id)
	if err != nil {
		return err
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}