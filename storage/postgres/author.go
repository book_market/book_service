package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/book_service/storage/repo"
)

type authorRepo struct {
	db *sqlx.DB
}

func NewAuthor(db *sqlx.DB) repo.AuthorStorageI {
	return &authorRepo{
		db: db,
	}
}

func (b *authorRepo) CreateAuthor(author *repo.Author) (*repo.Author, error) {
	query := `
		insert into authors (
			fullname
		) values ($1)
		returning id, created_at
	`

	row := b.db.QueryRow(
		query,
		author.Fullname,
	)

	err := row.Scan(
		&author.Id,
		&author.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return author, nil
}

func (b *authorRepo) GetAuthor(id int64) (*repo.Author, error) {
	var (
		result repo.Author
	)

	query := `
		select
			id,
			fullname,
			created_at
		from authors 
		where id = $1
	`

	row := b.db.QueryRow(query, id)
	err := row.Scan(
		&result.Id,
		&result.Fullname,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (b *authorRepo) GetAllAuthors(params *repo.GetAllAuthorsParams) (*repo.GetAllAuthorsResults, error) {
	result := repo.GetAllAuthorsResults{
		Authors: make([]*repo.Author, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" limit %d offset %d ", params.Limit, offset)

	filter := ""
	if params.Search != "" {
		str := "%" + params.Search + "%"
		filter += fmt.Sprintf(`
			where fullname ilike '%s'`,
		str,
		)
	}
	
	query := `
	select
		id,
		fullname,
		created_at
	from authors 
	` + filter + `order by created_at desc` + limit 

	rows, err := b.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var a repo.Author	
		err := rows.Scan(
			&a.Id,
			&a.Fullname,
			&a.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.Authors = append(result.Authors, &a)
	}

	queryCount := `select count(1) from authors ` + filter
	err = b.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (b *authorRepo) DeleteAuthor(id int64) error {
	query := `delete from authors where id = $1`

	res, err := b.db.Exec(query, id)
	if err != nil {
		return err
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}