package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/book_market/book_service/storage/repo"
)

func createCategory(t *testing.T) *repo.Category{
	c, err := strg.Category().CreateCategory(&repo.Category{
		Name: faker.FirstName(),
		ImageUrl: faker.URL(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, c)

	return c
}

func TestCreateCategory(t *testing.T) {
	createCategory(t)
}

func TestGetCategory(t *testing.T) {
	c := createCategory(t)

	category, err := strg.Category().GetCategory(c.Id)
	require.NoError(t, err)
	require.NotEmpty(t, category)
}

func TestGetAllCategories(t *testing.T) {
	category, err := strg.Category().GetAllCategories(&repo.GetAllCategoriesParams{
		Limit: 10,
		Page: 1,
		Search: "ab",
	})
	require.NoError(t, err)
	require.NotEmpty(t, category)
}
 
func TestDeleteCategory(t *testing.T) {
	c := createCategory(t)
	err := strg.Category().DeleteCategory(c.Id)
	require.NoError(t, err)
}