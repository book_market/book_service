package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/book_market/book_service/storage/repo"
)

func createReview(t *testing.T) *repo.Review{
	bookId := createBook(t)
	c, err := strg.Review().CreateReview(&repo.Review{
		BookId: bookId.Id,
		Review: faker.Word(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, c)

	return c
}

func TestCreatereview(t *testing.T) {
	createReview(t)
}

func TestGetreview(t *testing.T) {
	c := createReview(t)

	review, err := strg.Review().GetReview(c.Id)
	require.NoError(t, err)
	require.NotEmpty(t, review)
}

func TestGetAllReviews(t *testing.T) {
	review, err := strg.Review().GetAllReviews(&repo.GetAllReviewsParams{
		Limit: 10,
		Page: 1,
		Search: "ab",
	})
	require.NoError(t, err)
	require.NotEmpty(t, review)
}
 
func TestDeleteReview(t *testing.T) {
	c := createReview(t)
	err := strg.Review().DeleteReview(c.Id)
	require.NoError(t, err)
}