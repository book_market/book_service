package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/book_market/book_service/storage/repo"
)


func createAuthor(t *testing.T) *repo.Author {
	b, err := strg.Author().CreateAuthor(&repo.Author{
		Fullname: faker.FirstName(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, b)

	return b
}

func TestCreateAuthor(t *testing.T) {
	createAuthor(t)
}

func TestGetAuthor(t *testing.T) {
	c := createAuthor(t)

	author, err := strg.Author().GetAuthor(c.Id)
	require.NoError(t, err)
	require.NotEmpty(t, author)
}

func TestGetAllAuthors(t *testing.T) {
	author, err := strg.Author().GetAllAuthors(&repo.GetAllAuthorsParams{
		Limit: 10,
		Page: 1,
		Search: "ab",
	})
	require.NoError(t, err)
	require.NotEmpty(t, author)
}
 
func TestDeleteAuthor(t *testing.T) {
	c := createAuthor(t)
	err := strg.Author().DeleteAuthor(c.Id)
	require.NoError(t, err)
}