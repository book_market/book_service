package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/book_service/storage/repo"
)

type reviewRepo struct {
	db *sqlx.DB
}

func NewReview(db *sqlx.DB) repo.ReviewStorageI {
	return &reviewRepo{
		db: db,
	}
}

func (b *reviewRepo) CreateReview(review *repo.Review) (*repo.Review, error) {
	query := `
		insert into reviews (
			book_id,
			user_id,
			review
		) values ($1, $2, $3)
		returning id, created_at
	`

	row := b.db.QueryRow(
		query,
		review.BookId,
		review.CustomerId,
		review.Review,
	)

	err := row.Scan(
		&review.Id,
		&review.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return review, nil
}

func (b *reviewRepo) GetReview(id int64) (*repo.Review, error) {
	var (
		result repo.Review
	)

	query := `
		select
			id,
			book_id,
			user_id,
			review,
			created_at
		from reviews 
		where id = $1
	`

	row := b.db.QueryRow(query, id)
	err := row.Scan(
		&result.Id,
		&result.BookId,
		&result.CustomerId,
		&result.Review,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (b *reviewRepo) GetAllReviews(params *repo.GetAllReviewsParams) (*repo.GetAllReviewsResults, error) {
	result := repo.GetAllReviewsResults{
		Reviews: make([]*repo.Review, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" limit %d offset %d ", params.Limit, offset)

	filter := ""
	if params.Search != "" {
		str := "%" + params.Search + "%"
		filter += fmt.Sprintf(`
			where review ilike '%s'`,
		str,
		)
	}
	
	query := `
	select
		id,
		book_id,
		user_id,
		review,
		created_at
	from reviews 
	` + filter + `order by created_at desc` + limit 

	rows, err := b.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var a repo.Review	
		err := rows.Scan(
			&a.Id,
			&a.BookId,
			&a.CustomerId,
			&a.Review,
			&a.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.Reviews = append(result.Reviews, &a)
	}

	queryCount := `select count(1) from reviews ` + filter
	err = b.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (b *reviewRepo) DeleteReview(id int64) error {
	query := `delete from reviews where id = $1`

	res, err := b.db.Exec(query, id)
	if err != nil {
		return err
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (b *reviewRepo) UpdateReview(review *repo.Review) (*repo.Review, error) {
	query := `
		update reviews set
			book_id = $1,
			user_id = $2,
			review = $3
		where id = $4
	`

	row := b.db.QueryRow(
		query,
		review.BookId,
		review.CustomerId,
		review.Review,
		review.Id,
	)

	err := row.Scan(
		&review.BookId,
		&review.CustomerId,
		&review.Review,
	)
	if err != nil {
		return nil, err
	}

	return review, nil
}