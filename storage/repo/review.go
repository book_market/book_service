package repo

import "time"

type Review struct {
	Id         int64
	BookId     int64
	CustomerId int64
	Review     string
	CreatedAt  string
}

type GetAllReviewsParams struct {
	Limit  int32
	Page   int32
	Search string
}

type GetAllReviewsResults struct {
	Reviews []*Review
	Count   int32
}

type ReviewStorageI interface {
	CreateReview(a *Review) (*Review, error)
	GetReview(id int64) (*Review, error)
	// UpdateReview(a *Review) (*Review, error)
	GetAllReviews(params *GetAllReviewsParams) (*GetAllReviewsResults, error)
	DeleteReview(id int64) error
}

type UserInfo struct {
	ID          int64
	FirstName   string
	LastName    string
	Email       string
	Password    string
	PhoneNumber string
	ImageUrl    string
	Type        string
	CreatedAt   time.Time
}
