package repo

type Author struct {
	Id        int64
	Fullname  string
	CreatedAt string
}

type GetAllAuthorsParams struct {
	Limit  int32
	Page   int32
	Search string
}

type GetAllAuthorsResults struct {
	Authors []*Author
	Count   int32
}

type AuthorStorageI interface {
	CreateAuthor(a *Author) (*Author, error)
	GetAuthor(id int64) (*Author, error)
	GetAllAuthors(params *GetAllAuthorsParams) (*GetAllAuthorsResults, error)
	DeleteAuthor(id int64) error
}
