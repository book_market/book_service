package repo

type Category struct {
	Id        int64
	Name      string
	ImageUrl  string
	CreatedAt string
}

type GetAllCategoriesParams struct {
	Limit  int32
	Page   int32
	Search string
}

type GetAllCategoriesResults struct {
	Categories []*Category
	Count      int32
}

type CategoryStorageI interface {
	CreateCategory(a *Category) (*Category, error)
	GetCategory(id int64) (*Category, error)
	// UpdateCategory(a *Category) (*Category, error)
	GetAllCategories(params *GetAllCategoriesParams) (*GetAllCategoriesResults, error)
	DeleteCategory(id int64) error
}
