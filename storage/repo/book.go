package repo

type Book struct {
	Id            int64
	Name          string
	CategoryId    int64
	AuthorId      int64
	Description   string
	PublisherName string
	Language      string
	Inscription   string
	Page          int64
	Isbn          string
	Format        string
	Wrapper       string
	ImageUrl      string
	Price         float64
	CreatedAt     string
}

type GetAllBooksParams struct {
	Limit         int32
	Page          int32
	Name          string
	AuthorId      int64
	CategoryId    int64
	PublisherName string
	Language      string
	Inscription   string
	Wrapper       string
	PriceFrom     float64
	PriceTo       float64
}

type GetAllBooksResult struct {
	Books []*Book
	Count int32
}

type BookStorageI interface {
	CreateBook(u *Book) (*Book, error)
	GetBook(id int64) (*Book, error)
	GetAllBooks(params *GetAllBooksParams) (*GetAllBooksResult, error)
	DeleteBook(id int64) error
}
